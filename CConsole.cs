﻿using System;
using System.IO;

namespace HPB_Utility
{
  /// <summary>
  /// Valid Message types. Each gets its own color and will print as "OK", "ERROR" or "INFO".
  /// Highlight and Plain are "Info", Success is "Ok" and Error is... "Error".
  /// </summary>
  enum ConsoleMessageType
  {
    Plain,
    Highlight,
    Success,
    Error
  }

  class CConsole
  {
    /// <summary>
    /// Checks to see if the specified file exists. If it does, it deletes and recreates it,
    /// otherwise, it just creates a new file.
    /// </summary>
    /// <param name="pathToLog"></param>
    public static void ResetLogFile(string pathToLog)
    {
      try
      {
        if (File.Exists(pathToLog))
        {
          File.Delete(pathToLog);
        }
        var file = File.Create(pathToLog);
        file.Close();
      }
      catch (Exception ex)
      {
        WriteToConsole("Cannot Reset log file.", ConsoleMessageType.Error);
        WriteToConsole(ex.Message, ConsoleMessageType.Error);
      }
    }

    /// <summary>
    /// In addition to WriteToConsole(), this will log the same info, sans the color codes, to a specified
    /// log file.
    /// </summary>
    /// <param name="message">Message to Log/Echo</param>
    /// <param name="messageType">The ConsoleMessageType to use</param>
    /// <param name="pathToLog">Log file to write to</param>
    public static void LogAndWriteToConsole(string message, ConsoleMessageType messageType, string pathToLog)
    {
      try
      {
        var file = File.AppendText(pathToLog);
        file.Write(FormatString(message, messageType));
        file.Close();
        WriteToConsole(message, messageType);
      }
      catch (Exception ex)
      {
        WriteToConsole("Cannot write to log file.", ConsoleMessageType.Error);
        WriteToConsole(ex.Message, ConsoleMessageType.Error);
      }
    }

    /// <summary>
    /// Used by LogAndWriteToConsole and WriteToConsole to correctly format the provided message for display.
    /// </summary>
    /// <param name="message"></param>
    /// <param name="messageType"></param>
    /// <returns></returns>
    private static string FormatString(string message, ConsoleMessageType messageType)
    {
      string output = String.Empty;

      switch (messageType)
      {
        case ConsoleMessageType.Success:
          output = String.Format("[{0, -6}][{1, -22}] {2}", "OK", DateTime.Now.ToString("G"), message + Environment.NewLine);
          break;
        case ConsoleMessageType.Error:
          output = String.Format("[{0, -6}][{1, -22}] {2}", "ERROR ", DateTime.Now.ToString("G"), message + Environment.NewLine);
          break;
        case ConsoleMessageType.Plain:
        case ConsoleMessageType.Highlight:
          output = String.Format("[{0, -6}][{1, -22}] {2}", "INFO ", DateTime.Now.ToString("G"), message + Environment.NewLine);
          break;
      }

      return output;
    }

    /// <summary>
    /// Echo out the provided message at the provided severity out to the console.
    /// </summary>
    /// <param name="msg"></param>
    /// <param name="type"></param>
    public static void WriteToConsole(string msg, ConsoleMessageType type)
    {
      switch (type)
      {
        case ConsoleMessageType.Success:
          Console.ForegroundColor = ConsoleColor.Green;
          Console.Write(FormatString(msg, type));
          break;

        case ConsoleMessageType.Error:
          Console.ForegroundColor = ConsoleColor.Red;
          Console.Write(FormatString(msg, type));
          break;

        case ConsoleMessageType.Highlight:
          Console.ForegroundColor = ConsoleColor.Yellow;
          Console.Write(FormatString(msg, type));
          break;

        case ConsoleMessageType.Plain:
        default:
          Console.ForegroundColor = ConsoleColor.Gray;
          Console.Write(FormatString(msg, type));
          break;
      }

      Console.ResetColor();
    }
  }
}
