﻿using System;
using System.Collections.Generic;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Chrome;
using HPB_Utility;
using System.Diagnostics;

namespace HotMoney
{

  /// <summary>
  /// Currently supported browsers that can run headless
  /// </summary>
  enum BrowserType
  {
    FireFox,
    Chrome
  }

  /// <summary>
  /// The body of the crawler.  This spider does not leave the domain specified by the starting URL.
  /// It will, however, crawl everything it can find on that domain.
  /// </summary>
  class Spider
  {
    private List<string> VisitedUrls { get; set; }
    private Queue<string> UrlsToVisit { get; set; }
    private Uri BaseUri { get; set; }
    private RemoteWebDriver Browser { get; set; }
    private BrowserType BrowserType { get; set; }
    private string LogPath { get; set; }

    /// <summary>
    /// Class Constructor
    /// </summary>
    /// <param name="startingUrl">Fully Qualified URL, including scheme, to crawl. E.g "https://www.myurl.com"</param>
    /// <param name="browserType">Supported BrowserType</param>
    /// <param name="pathToLog">Path to the log file we'll write. This is required. It would be best to supply a fully qualified path.</param>
    public Spider(string startingUrl, BrowserType browserType, string pathToLog)
    {
      VisitedUrls = new List<string>();
      UrlsToVisit = new Queue<string>();
      BaseUri = new Uri(startingUrl);
      LogPath = pathToLog;

      // Set up the browser options for each supported browser
      BrowserType = browserType;
      switch (browserType)
      {
        case BrowserType.FireFox:
          FirefoxOptions firefoxOptions = new FirefoxOptions();
          firefoxOptions.AddArgument("-headless");
          Browser = new FirefoxDriver(firefoxOptions);
          break;
        case BrowserType.Chrome:
          ChromeOptions chromeOptions = new ChromeOptions();
          chromeOptions.AddArgument("headless");
          chromeOptions.AddArgument("log-level=3");
          chromeOptions.AddArgument("disable-gpu");
          Browser = new ChromeDriver(chromeOptions);
          break;
      }

      // Prep the log file
      CConsole.ResetLogFile(LogPath);
      CConsole.WriteToConsole("Logging to " + LogPath, ConsoleMessageType.Plain);

      // Add the starting URL to our URL queue so we have something to do.
      UrlsToVisit.Enqueue(startingUrl);
    }

    /// <summary>
    /// Begin the crawling process. Uses the startingUrl as the beginning.  Keeps track of URLs it finds, and URL's it's visited,
    /// so that it doesn't repeat itself.
    /// </summary>
    public void Crawl()
    {
      // Too many errors (connection errors, mostly - currently 5) will cause th crawler to abandon its efforts.
      int errorCount = 0;

      // We alsready have 1 URL in the queue since that happens in the constructor. 
      while (UrlsToVisit.Count > 0)
      {
        CConsole.LogAndWriteToConsole(new String('-', 75), ConsoleMessageType.Plain, LogPath);
        CConsole.LogAndWriteToConsole($"STATS --- VISITED: {VisitedUrls.Count}, QUEUED: {UrlsToVisit.Count}", ConsoleMessageType.Plain, LogPath);

        // Pop a URL off the top of the queue
        var url = UrlsToVisit.Dequeue();

        try
        {
          int newUrlCount = 0;

          // Keep track of how long it takes. We geep the stopwatch going until we've gathered all the links from the DOM, as the dom will have to be fully
          // painted in order for us to do this piece of work.
          var stopwatch = new Stopwatch();
          CConsole.LogAndWriteToConsole("Trying to visit: " + url, ConsoleMessageType.Highlight, LogPath);
          stopwatch.Start();
          Browser.Navigate().GoToUrl(url);
          VisitedUrls.Add(url);

          // we don't currenly have a good way of checking the status header, but we have a custom 404 page that's displayed,
          // so we can look at it's page title as an indicator of not-found-ness
          var title = Browser.Title;
          if (title.StartsWith("404"))
          {
            CConsole.LogAndWriteToConsole("This URL is probably returning an error 404!", ConsoleMessageType.Error, LogPath);
          }
          CConsole.LogAndWriteToConsole("Finding Links...", ConsoleMessageType.Plain, LogPath);
          var links = Browser.FindElementsByTagName("a");
          
          // Stop the clock and display/log how long this page took
          stopwatch.Stop();
          CConsole.LogAndWriteToConsole($"Visit completed in {stopwatch.ElapsedMilliseconds} milliseconds", ConsoleMessageType.Plain, LogPath);

          // Parse through every found URL and:
          // * verify it's followable (must be a valid href link, no tel: or mailto: or javascript:, etc)
          // * make sure it's on-domain
          // * make sure it's not an on-page nav link
          // * make sure it's not to a PDF
          // * make sure it's not already been visited
          // * add it to our visit queue
          foreach (var link in links)
          {
            try
            {
              string linkUrl = link.GetAttribute("href");

              #region ErrorHandling
              if (String.IsNullOrEmpty(linkUrl))
              {
                continue;
              }

              var parsedUrl = new Uri(linkUrl);

              if (parsedUrl.Scheme != "http" && parsedUrl.Scheme != "https")
              {
                continue;
              }

              if (parsedUrl.Host != BaseUri.Host)
              {
                continue;
              }

              if (parsedUrl.OriginalString.Contains("#"))
              {
                continue;
              }

              if (parsedUrl.OriginalString.EndsWith(".pdf"))
              {
                continue;
              }
              #endregion

              // Work with the URL
              if (!VisitedUrls.Contains(linkUrl) && !UrlsToVisit.Contains(linkUrl))
              {
                CConsole.LogAndWriteToConsole($"Adding {linkUrl} to queue...", ConsoleMessageType.Plain, LogPath);
                newUrlCount++;
                UrlsToVisit.Enqueue(linkUrl);
              }
            }
            catch (Exception ex)
            {
              CConsole.LogAndWriteToConsole("Err 200: There was a problem getting links on this page. We will not retry this URL.", ConsoleMessageType.Error, LogPath);
              CConsole.LogAndWriteToConsole(ex.Message, ConsoleMessageType.Error, LogPath);
            }
          }
          CConsole.LogAndWriteToConsole($"Visit successful. {newUrlCount} URLs added to the queue.", ConsoleMessageType.Success, LogPath);
          CConsole.LogAndWriteToConsole(new String('-', 75), ConsoleMessageType.Plain, LogPath);
        }
        catch (Exception ex)
        {
          errorCount++;
          CConsole.LogAndWriteToConsole($"Err 100 ({errorCount}): There was a problem connecting to the server.", ConsoleMessageType.Error, LogPath);
          CConsole.LogAndWriteToConsole("URL: " + url, ConsoleMessageType.Error, LogPath);
          CConsole.LogAndWriteToConsole(ex.Message, ConsoleMessageType.Error, LogPath);

          //Re-add the url to the queue
          UrlsToVisit.Enqueue(url);

          if (errorCount > 5)
          {
            Browser.Quit();
            throw new Exception("Too many connection errors. Abandoning crawl.");
          }
        }
      }

      Browser.Quit();
    }
  }
}
