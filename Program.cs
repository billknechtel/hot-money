﻿using System;
using HPB_Utility;
using Mono.Options;

namespace HotMoney
{
  class Program
  {
    static void Main(string[] args)
    {
      var beginTime = DateTime.Now;

      // Prep and build up our console options
      var rootUrl = String.Empty;
      var logPath = String.Empty;
      var showHelp = false;

      var options = new OptionSet
      {
        { "u|url=", "the root url from which to begin our crawl.", u => rootUrl = u },
        { "l|logpath=", "the path to the log we will capture.", l => logPath = l },
        { "h|help", "show this message and exit", h => showHelp = h != null },
      };

      try
      {
        // Option parsing
        _ = options.Parse(args);

        if (showHelp)
        {
          ShowHelp(options);
          System.Environment.Exit(0);
        }
        if (String.IsNullOrEmpty(rootUrl))
        {
          throw new OptionException("A required option is missing.", "url");
        }
        if (String.IsNullOrEmpty(logPath))
        {
          throw new OptionException("A required option is missing.", "logpath");
        }

        // TODO: Allow the browser type to be switched via CLI option
        // As of the time of this writing the FireFox selenium driver I use is super slow when it comes to parsing off the
        // links of a page.
        var spider = new Spider(rootUrl, BrowserType.Chrome, logPath);

        try
        {
          // Magic
          spider.Crawl();
        }
        catch (Exception ex)
        {
          var errorEndTime = DateTime.Now;
          var errorTotalTime = errorEndTime.Subtract(beginTime);
          CConsole.LogAndWriteToConsole("Fatal Error: " + ex.Message, ConsoleMessageType.Error, logPath);
          CConsole.LogAndWriteToConsole($"Crawl Terminated.  Total Time: {errorTotalTime}", ConsoleMessageType.Error, logPath);
          System.Environment.Exit(1);
        }

        // Calculate & print how long the program took to run, exit.
        var endTime = DateTime.Now;
        var totalTime = endTime.Subtract(beginTime);
        CConsole.LogAndWriteToConsole($"Crawl complete, the money is hot. Total time: {totalTime}", ConsoleMessageType.Success, logPath);

        System.Environment.Exit(0);
      }
      catch (OptionException ex)
      {
        Console.WriteLine("HotMoney: " + ex.Message);
        Console.WriteLine("Try `HotMoney --help` for more information.");
        System.Environment.Exit(1);
      }
    }

    /// <summary>
    /// Print the Help screen
    /// </summary>
    /// <param name="options">Pass in options built up from Main()</param>
    static void ShowHelp(OptionSet options)
    {
      Console.WriteLine("Usage: HotMoney.exe [OPTIONS]");
      Console.WriteLine("NOTE: `url` and `logpath` are rquired options.");
      Console.WriteLine();
      Console.WriteLine("Starts a crawl of the website specified at `url`.");
      Console.WriteLine("Echoes out its progress to the console, and logs the same to the");
      Console.WriteLine("file specified at `logpath`");
      Console.WriteLine();
      Console.WriteLine("Options:");
      options.WriteOptionDescriptions(Console.Out);
      Console.WriteLine();
    }
  }
}
